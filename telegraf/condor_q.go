package main

import (
	"fmt"
	"os/exec"
	"regexp"
	"strings"
)

func main() {
	var re = regexp.MustCompile(`(?m)(?P<jobs>\d+\s*jobs);\s*(?P<completed>\d+\s*completed),\s*(?P<removed>\d+\s*removed),\s*(?P<idle>\d+\s*idle),\s*(?P<running>\d+\s*running),\s*(?P<held>\d+\s*held),\s*(?P<suspended>\d+\s*suspended)`)
	c := exec.Command("condor_q")
	outputString, _ := c.Output()
	// 	var outputString = `-- Schedd: rocks-186.sdsc.edu : <172.30.0.200:52641?...
	//  ID      OWNER            SUBMITTED     RUN_TIME ST PRI SIZE CMD

	// 1 jobs; 2 completed, 3 removed, 4 idle, 5 running, 6 held, 7 suspended`

	var regexGroupMatch = re.FindAllStringSubmatch(string(outputString), -1)
	fmt.Printf("GroupName: %s\n", regexGroupMatch[0][0])
	fmt.Printf("%d\n", len(regexGroupMatch[0]))
	for i := 1; i < len(regexGroupMatch[0]); i++ {
		// fmt.Printf("%s\n", regexGroupMatch[0][i])
		var matched = strings.Split(regexGroupMatch[0][i], " ")
		fmt.Printf("length %d: arr[0] = %s, arr[1] = %s\n", len(matched), matched[0], matched[1])
	}

	fmt.Printf("%s", outputString)
}
